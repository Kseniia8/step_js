
document.addEventListener('DOMContentLoaded', () => {
    const trainers = [
        {
            "first name": "Олексій",
            "last name": "Петров",
            photo: "./img/trainers/trainer-m1.jpg",
            specialization: "Басейн",
            category: "майстер",
            experience: 8,
            description: "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань."
        },
        {
            "first name": "Марина",
            "last name": "Іванова",
            photo: "./img/trainers/trainer-f1.png",
            specialization: "Тренажерний зал",
            category: "спеціаліст",
            experience: 2,
            description: "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами."
        },
        {
            "first name": "Ігор",
            "last name": "Сидоренко",
            photo: "./img/trainers/trainer-m2.jpg",
            specialization: "Дитячий клуб",
            category: "інструктор",
            experience: 1,
            description: "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків."
        },
        {
            "first name": "Тетяна",
            "last name": "Мороз",
            photo: "./img/trainers/trainer-f2.jpg",
            specialization: "Бійцівський клуб",
            category: "майстер",
            experience: "10 років",
            description:
                "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
        },
        {
            "first name": "Сергій",
            "last name": "Коваленко",
            photo: "./img/trainers/trainer-m3.jpg",
            specialization: "Тренажерний зал",
            category: "інструктор",
            experience: "1 рік",
            description:
                "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
        },
        {
            "first name": "Олена",
            "last name": "Лисенко",
            photo: "./img/trainers/trainer-f3.jpg",
            specialization: "Басейн",
            category: "спеціаліст",
            experience: "4 роки",
            description:
                "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
        },
        {
            "first name": "Андрій",
            "last name": "Волков",
            photo: "./img/trainers/trainer-m4.jpg",
            specialization: "Бійцівський клуб",
            category: "інструктор",
            experience: "1 рік",
            description:
                "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
        },
        {
            "first name": "Наталія",
            "last name": "Романенко",
            photo: "./img/trainers/trainer-f4.jpg",
            specialization: "Дитячий клуб",
            category: "спеціаліст",
            experience: "3 роки",
            description:
                "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
        },
        {
            "first name": "Віталій",
            "last name": "Козлов",
            photo: "./img/trainers/trainer-m5.jpg",
            specialization: "Тренажерний зал",
            category: "майстер",
            experience: "10 років",
            description:
                "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
        },
        {
            "first name": "Юлія",
            "last name": "Кравченко",
            photo: "./img/trainers/trainer-f5.jpg",
            specialization: "Басейн",
            category: "спеціаліст",
            experience: "4 роки",
            description:
                "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
        },
        {
            "first name": "Олег",
            "last name": "Мельник",
            photo: "./img/trainers/trainer-m6.jpg",
            specialization: "Бійцівський клуб",
            category: "майстер",
            experience: "12 років",
            description:
                "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
        },
        {
            "first name": "Лідія",
            "last name": "Попова",
            photo: "./img/trainers/trainer-f6.jpg",
            specialization: "Дитячий клуб",
            category: "інструктор",
            experience: "1 рік",
            description:
                "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
        },
        {
            "first name": "Роман",
            "last name": "Семенов",
            photo: "./img/trainers/trainer-m7.jpg",
            specialization: "Тренажерний зал",
            category: "спеціаліст",
            experience: "2 роки",
            description:
                "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
        },
        {
            "first name": "Анастасія",
            "last name": "Гончарова",
            photo: "./img/trainers/trainer-f7.jpg",
            specialization: "Басейн",
            category: "інструктор",
            experience: "1 рік",
            description:
                "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
        },
        {
            "first name": "Валентин",
            "last name": "Ткаченко",
            photo: "./img/trainers/trainer-m8.jpg",
            specialization: "Бійцівський клуб",
            category: "спеціаліст",
            experience: "2 роки",
            description:
                "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
        },
        {
            "first name": "Лариса",
            "last name": "Петренко",
            photo: "./img/trainers/trainer-f8.jpg",
            specialization: "Дитячий клуб",
            category: "майстер",
            experience: "7 років",
            description:
                "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
        },
        {
            "first name": "Олексій",
            "last name": "Петров",
            photo: "./img/trainers/trainer-m9.jpg",
            specialization: "Басейн",
            category: "майстер",
            experience: "11 років",
            description:
                "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
        },
        {
            "first name": "Марина",
            "last name": "Іванова",
            photo: "./img/trainers/trainer-f9.jpg",
            specialization: "Тренажерний зал",
            category: "спеціаліст",
            experience: "2 роки",
            description:
                "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
        },
        {
            "first name": "Ігор",
            "last name": "Сидоренко",
            photo: "./img/trainers/trainer-m10.jpg",
            specialization: "Дитячий клуб",
            category: "інструктор",
            experience: "1 рік",
            description:
                "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
        },
        {
            "first name": "Наталія",
            "last name": "Бондаренко",
            photo: "./img/trainers/trainer-f10.jpg",
            specialization: "Бійцівський клуб",
            category: "майстер",
            experience: "8 років",
            description:
                "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
        },
        {
            "first name": "Андрій",
            "last name": "Семенов",
            photo: "./img/trainers/trainer-m11.jpg",
            specialization: "Тренажерний зал",
            category: "інструктор",
            experience: "1 рік",
            description:
                "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
        },
        {
            "first name": "Софія",
            "last name": "Мельник",
            photo: "./img/trainers/trainer-f11.jpg",
            specialization: "Басейн",
            category: "спеціаліст",
            experience: "6 років",
            description:
                "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
        },
        {
            "first name": "Дмитро",
            "last name": "Ковальчук",
            photo: "./img/trainers/trainer-m12.png",
            specialization: "Дитячий клуб",
            category: "майстер",
            experience: "10 років",
            description:
                "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
        },
        {
            "first name": "Олена",
            "last name": "Ткаченко",
            photo: "./img/trainers/trainer-f12.jpg",
            specialization: "Бійцівський клуб",
            category: "спеціаліст",
            experience: "5 років",
            description:
                "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
        },
    ];





const container = document.querySelector('.trainers-cards__container');
const modalTemplate = document.getElementById('modal-template');
const preloader = document.getElementById('preloader');

function renderTrainers(trainers) {
    container.innerHTML = '';
    trainers.forEach((trainer, index) => {
        const card = document.createElement('li');
        card.classList.add('trainer');
        card.innerHTML = `
            <img src="${trainer.photo}" alt="${trainer["first name"]} ${trainer["last name"]}" class="trainer__img" width="280" height="300"/>
            <p class="trainer__name">${trainer["first name"]} ${trainer["last name"]}</p>
            <p class="trainer__experience">Досвід: ${trainer.experience} років</p>
            <p class="trainer__category">Категорія: ${trainer.category}</p>
            <p class="trainer__specialization">Напрям: ${trainer.specialization}</p>
            <p class="trainer__description">${trainer.description}</p>
            <button class="trainer__show-more" type="button" data-index="${index}">ПОКАЗАТИ</button>
        `;
        container.appendChild(card);
    });

    document.querySelectorAll('.trainer__show-more').forEach(button => {
        button.addEventListener('click', (event) => {
            const index = event.currentTarget.getAttribute('data-index');
            showModal(trainers[index]);
        });
    });
}

function showModal(trainer) {
    const modalContent = modalTemplate.content.cloneNode(true);
    modalContent.querySelector('.modal__img').src = trainer.photo;
    modalContent.querySelector('.modal__name').textContent = `${trainer["first name"]} ${trainer["last name"]}`;
    modalContent.querySelector('.modal__point--category').textContent = `Категорія: ${trainer.category}`;
    modalContent.querySelector('.modal__point--experience').textContent = `Досвід: ${trainer.experience} років`;
    modalContent.querySelector('.modal__point--specialization').textContent = `Напрям: ${trainer.specialization}`;
    modalContent.querySelector('.modal__text').textContent = trainer.description;

    const modalElement = modalContent.querySelector('.modal');
    const closeModal = () => {
        document.body.removeChild(modalElement);
        document.body.style.overflow = 'auto';
    };

    modalContent.querySelector('.modal__close').addEventListener('click', closeModal);

    document.body.appendChild(modalElement);
    document.body.style.overflow = 'hidden';
}

function sortTrainers(criteria) {
    let sortedTrainers;
    if (criteria === 'last-name') {
        sortedTrainers = trainers.slice().sort((a, b) => a["last name"].localeCompare(b["last name"]));
    } else if (criteria === 'experience') {
        sortedTrainers = trainers.slice().sort((a, b) => b.experience - a.experience);
    } else {
        sortedTrainers = trainers;
    }
    renderTrainers(sortedTrainers);
}

document.querySelectorAll('.sorting__btn').forEach(button => {
    button.addEventListener('click', () => {
        document.querySelector('.sorting__btn--active').classList.remove('sorting__btn--active');
        button.classList.add('sorting__btn--active');
        const sortCriteria = button.getAttribute('data-sort');
        sortTrainers(sortCriteria);
    });
});

// Initial render
renderTrainers(trainers);

});

function showPreloader() {
    preloader.style.display = 'block';
}

function hidePreloader() {
    preloader.style.display = 'none';





    //  .

    document.addEventListener('DOMContentLoaded', () => {
        const sortButtons = document.querySelectorAll('.sorting__btn');
    
        // Проверка на существование элементов с классом .sorting__btn
        if (sortButtons.length === 0) {
            console.error('Элементы с классом .sorting__btn не найдены.');
            return;
        }
    
        sortButtons.forEach(button => {
            button.addEventListener('click', () => {
                const activeButton = document.querySelector('.sorting__btn--active');
                // Проверка на существование активного элемента
                if (activeButton) {
                    activeButton.classList.remove('sorting__btn--active');
                }
                button.classList.add('sorting__btn--active');
                const sortCriteria = button.getAttribute('data-sort');
                sortTrainers(sortCriteria);
            });
        });
    
        function sortTrainers(type) {
            const trainerCards = document.querySelectorAll('.trainer');
            let sortedCards = Array.from(trainerCards);
            
            sortedCards.sort((a, b) => {
                if (type === 'default') return 0;
                if (type === 'last-name') {
                    const nameA = a.getAttribute('data-last-name').toLowerCase();
                    const nameB = b.getAttribute('data-last-name').toLowerCase();
                    return nameA.localeCompare(nameB);
                }
                if (type === 'experience') {
                    const expA = parseInt(a.getAttribute('data-experience'), 10);
                    const expB = parseInt(b.getAttribute('data-experience'), 10);
                    return expB - expA;
                }
            });
            
            const container = document.querySelector('.trainers-cards__container');
            container.innerHTML = '';
            sortedCards.forEach(card => container.appendChild(card));
        }
    });
    
}


// .

// function renderTrainers(trainers) {
//     showPreloader();
//     setTimeout(() => {
//         container.innerHTML = '';
//         trainers.forEach((trainer, index) => {
//             const card = document.createElement('li');
//             card.classList.add('trainer');
//             card.innerHTML = `
//                 <img src="${trainer.photo}" alt="${trainer["first name"]} ${trainer["last name"]}" class="trainer__img" width="280" height="300"/>
//                 <p class="trainer__name">${trainer["first name"]} ${trainer["last name"]}</p>
//                 <p class="trainer__experience">Досвід: ${trainer.experience} років</p>
//                 <p class="trainer__category">Категорія: ${trainer.category}</p>
//                 <p class="trainer__specialization">Напрям: ${trainer.specialization}</p>
//                 <p class="trainer__description">${trainer.description}</p>
//                 <button class="trainer__show-more" type="button" data-index="${index}">ПОКАЗАТИ</button>
//             `;
//             container.appendChild(card);
//         });

//         document.querySelectorAll('.trainer__show-more').forEach(button => {
//             button.addEventListener('click', (event) => {
//                 const index = event.currentTarget.getAttribute('data-index');
//                 showModal(trainers[index]);
//             });
//         });

//         hidePreloader();
//     }, 1000); // Simulate loading time
// };

//     document.querySelectorAll('.sorting__btn').forEach(button => {
//         button.addEventListener('click', () => {
//             document.querySelector('.sorting__btn--active').classList.remove('sorting__btn--active');
//             button.classList.add('sorting__btn--active');
//             const sortCriteria = button.getAttribute('data-sort');
//             sortTrainers(sortCriteria);

//         });
//     });



// 

    window.addEventListener('load', function () {
        const preloader = document.getElementById('preloader');
        preloader.classList.add('hidden');
    });





    document.addEventListener('DOMContentLoaded', () => {
        const filterForm = document.querySelector('.filters');
        const trainerCards = document.querySelectorAll('.trainer');

        // Обработка отправки формы фильтрации
        filterForm.addEventListener('submit', (event) => {
            event.preventDefault();
            filterTrainers();
        });

        function filterTrainers() {
            const selectedDirection = document.querySelector('input[name="direction"]:checked').value;
            const selectedCategory = document.querySelector('input[name="category"]:checked').value;

            trainerCards.forEach(card => {
                const cardDirection = card.getAttribute('data-direction');
                const cardCategory = card.getAttribute('data-category');
                const directionMatch = selectedDirection === 'all' || selectedDirection === cardDirection;
                const categoryMatch = selectedCategory === 'all' || selectedCategory === cardCategory;

                if (directionMatch && categoryMatch) {
                    card.style.display = 'block';
                } else {
                    card.style.display = 'none';
                }
            });
        }
    });



    